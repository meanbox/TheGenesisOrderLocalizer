package main

import (
	"fmt"
	"os"
	"path/filepath"
	"regexp"
	"strings"

	translator "github.com/Conight/go-googletrans"
	"github.com/xuri/excelize/v2"
)

var (
	dataPath = ""
	outPath  = ""
)

func main() {
	if len(os.Args) < 2 {
		panic("必须传入 data 数据目录")
	}
	dataPath = os.Args[1]
	entrys, e := os.ReadDir(dataPath)
	if e != nil {
		panic(fmt.Sprintf("读取目录 %s 失败：%v", dataPath, e))
	}

	trans := translator.New()
	for _, entry := range entrys {
		if !entry.IsDir() {
			if strings.HasSuffix(entry.Name(), ".csv") {
				if e := translateCsvFile(filepath.Join(dataPath, entry.Name()), trans); e != nil {
					panic(fmt.Sprintf("翻译 %s 失败：%v", entry.Name(), e))
				}
			} else if strings.HasSuffix(entry.Name(), ".xlsx") {
				if e := translateXlsxFile(filepath.Join(dataPath, entry.Name()), trans); e != nil {
					panic(fmt.Sprintf("翻译 %s 失败：%v", entry.Name(), e))
				}
			}
		}
	}

	fmt.Println("\n完成翻译。")
}

const (
	keepIgnored = true
	colTarget   = "B"
)

var (
	rexIgnore       = regexp.MustCompile(`([A-Z]{2,}|[\/\\<>]|(""))`)
	rexIgnoreNative = regexp.MustCompile(`[a-z][A-Z][a-z0-9]`)
)

func translateCsvFile(file string, trans *translator.Translator) error {
	fi, _ := os.Stat(file)
	fmod := fi.Mode()
	data, e := os.ReadFile(file)
	if e != nil {
		return e
	}
	lines := strings.Split(strings.TrimSpace(string(data)), "\r\n")
	fmt.Printf("\n翻译 %s 共 %d 行：\n", filepath.Base(file), len(lines)-1)
	words := []string{}
	lineNos := []int{}
	prefixes := []string{}
	fieldList := [][]string{}
	for i := 1; i < len(lines); i++ {
		line := strings.TrimSpace(lines[i])
		if len(line) == 0 {
			fmt.Print("|")
			continue
		}
		fields := strings.Split(line, ",")
		origin := fields[0][1 : len(fields[0])-1] // 去除首尾引号
		if strings.HasPrefix(origin, "EV") || rexIgnore.MatchString(origin) {
			fmt.Print("|")
			continue
		}

		prefix := ""
		if p := strings.Index(origin, "."); p < len(origin)-1 {
			prefix = origin[:p+1]
		} else if p := strings.Index(origin, ","); p < len(origin)-1 {
			prefix = origin[:p+1]
		}
		content := origin[len(prefix):]
		if rexIgnoreNative.MatchString(content) {
			fmt.Print("|")
			continue
		}

		if len(strings.TrimSpace(content)) > 0 {
			words = append(words, content)
			lineNos = append(lineNos, i)
			prefixes = append(prefixes, prefix)
			fieldList = append(fieldList, fields)
		}

		fmt.Print(".")
	}

	groupSize := 30
	for index := 0; index < len(words); index += groupSize {
		var nextWords []string
		if (index + groupSize) < len(words) {
			nextWords = words[index : index+groupSize]
		} else {
			nextWords = words[index:]
		}

		result, e := trans.Translate(strings.Join(nextWords, "\n"), "en", "zh-cn")
		if e != nil {
			panic(e)
		}
		// 回写
		translated := strings.Split(result.Text, "\n")
		if len(translated) != len(nextWords) {
			panic(fmt.Sprintf("翻译结果 %d 行与原文 %d 行不一致", len(translated), len(nextWords)))
		}
		for i := 0; i < len(translated); i++ {
			idx := index + i
			fieldList[idx][1] = fmt.Sprintf("\"%s%s\"", prefixes[idx], translated[i])
			lines[lineNos[idx]] = strings.Join(fieldList[idx], ",")
		}
	}

	return os.WriteFile(filepath.Join(outPath, filepath.Base(file)), []byte(strings.Join(lines, "\r\n")+"\r\n"), fmod)
}

type translation struct {
	rowNo      int
	source     string
	translated string
}

type translationGroup struct {
	mergedPrefix  []string
	mergedSuffix  []string
	mergedContent []string
	translations  []*translation
}

func translateXlsxFile(file string, trans *translator.Translator) error {
	fi, _ := os.Stat(file)
	fmod := fi.Mode()
	xlsxFile, e := os.OpenFile(file, os.O_RDWR, fmod)
	if e != nil {
		return e
	}
	xlsx, e := excelize.OpenFile(file)
	if e != nil {
		return e
	}
	defer func() {
		xlsx.Close()
		xlsxFile.Close()
	}()

	sheet := xlsx.WorkBook.Sheets.Sheet[0]
	rows, e := xlsx.GetRows(sheet.Name)
	if e != nil {
		return e
	}
	fmt.Printf("\n翻译 %s 共 %d 行：\n", filepath.Base(file), len(rows))

	groupSize := 30
	groups := []*translationGroup{}
	group := &translationGroup{
		mergedPrefix: []string{},
		mergedSuffix: []string{},
		translations: []*translation{},
	}
	for rowNo, row := range rows {
		source := strings.TrimSpace(row[0])
		if strings.HasPrefix(source, "EV") || strings.Contains(source, `\\`) {
			if keepIgnored {
				xlsx.SetCellStr(sheet.Name, fmt.Sprintf("%s%d", colTarget, rowNo+1), row[0])
			}
			fmt.Print("|")
			continue
		}

		prefix := ""
		var p int
		if p = strings.Index(source, "."); p < 10 {
			prefix = source[:p+1]
		} else if p = strings.Index(source, ","); p < 10 {
			prefix = source[:p+1]
		}
		content :=
			strings.ReplaceAll(
				strings.ReplaceAll(
					strings.ReplaceAll(
						strings.ReplaceAll(
							strings.ReplaceAll(
								strings.ReplaceAll(strings.ReplaceAll(source[len(prefix):], "\r\n", "\n"), "\n", " ^ "),
								"<", " <",
							),
							">", "> ",
						),
						"\\", " \\",
					),
					"]", "] ",
				),
				"/", " /",
			)
		// if strings.Contains(content, "\\") {
		// 	fmt.Println()
		// 	fmt.Println("`" + content + "`")
		// 	fmt.Println()
		// }

		// if rexIgnoreNative.MatchString(content) || rexIgnore.MatchString(content) {
		// 	fmt.Println()
		// 	fmt.Println(source)
		// 	fmt.Printf("p=%d, prefix=`%s` content=`%s`, nativeMatch=%v, ignoreMatch=%v\n", p, prefix, content, rexIgnoreNative.MatchString(content), rexIgnore.MatchString(content))
		// 	if keepIgnored {
		// 		xlsx.SetCellStr(sheet.Name, fmt.Sprintf("%s%d", colTarget, rowNo+1), row[0])
		// 	}
		// 	fmt.Print("|")
		// 	continue
		// }
		if len(strings.TrimSpace(content)) == 0 {
			if keepIgnored {
				xlsx.SetCellStr(sheet.Name, fmt.Sprintf("%s%d", colTarget, rowNo+1), row[0])
			}
			fmt.Print("|")
			continue
		}

		if len(group.translations) >= groupSize {
			groups = append(groups, group)
			group = &translationGroup{
				mergedPrefix: []string{},
				mergedSuffix: []string{},
				translations: []*translation{},
			}
		}
		group.mergedPrefix = append(group.mergedPrefix, prefix)
		group.mergedSuffix = append(group.mergedSuffix, "")
		group.mergedContent = append(group.mergedContent, content)
		group.translations = append(group.translations, &translation{
			rowNo:      rowNo + 1,
			source:     source,
			translated: "",
		})

		fmt.Print(".")
	}

	if len(group.translations) > 0 {
		groups = append(groups, group)
	}

	for _, group := range groups {
		result, e := trans.Translate(strings.Join(group.mergedContent, "\n"), "en", "zh-cn")
		if e != nil {
			panic(e)
		}
		// result := &struct {
		// 	Text string
		// }{
		// 	Text: strings.Join(group.mergedContent, "\n"),
		// }
		translated := strings.Split(result.Text, "\n")
		if len(group.mergedContent) != len(translated) {
			panic(fmt.Sprintf("翻译结果 %d 行与原文 %d 行不一致", len(translated), len(group.mergedContent)))
		}
		for i := range translated {
			translated[i] = strings.ReplaceAll(translated[i], "^", "\n")
		}
		for i, translation := range group.translations {
			if e := xlsx.SetCellStr(
				sheet.Name,
				fmt.Sprintf("%s%d", colTarget, translation.rowNo),
				fmt.Sprintf("%s%s%s", group.mergedPrefix[i], translated[i], group.mergedSuffix[i]),
			); e != nil {
				panic(e)
			}
		}
	}

	_, e = xlsx.WriteTo(xlsxFile)
	return e
}
